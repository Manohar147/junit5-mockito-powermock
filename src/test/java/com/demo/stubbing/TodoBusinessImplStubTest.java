package com.demo.stubbing;

import com.demo.mockito.TodoBusinessImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Without mockito traditionally we using stubbing where
 * we create a dummy implementation of the service are return dummy values.
 * But Imagine How complicated this would get as we want to test retrieveTodosRelatedToSpring with multiple users.
 * We must write logic to handle the same.
 *
 * Check TodoServiceStub.
 */
public class TodoBusinessImplStubTest {

  @Test public void usingStub() {
    TodoBusinessImpl business = new TodoBusinessImpl(new TodoServiceStub());
    List<String> filtered = business.retrieveTodosRelatedToSpring("Dummy");
    assertEquals(2, filtered.size());
  }
}