package com.demo.stubbing;

import com.demo.mockito.TodoService;

import java.util.Arrays;
import java.util.List;

public class TodoServiceStub implements TodoService {
  @Override public List<String> retrieveTodos(String user) {
    //    if ("Dummy".equalsIgnoreCase(user)) {
    //
    //    } else if ("Dummy1".equalsIgnoreCase(user)) {
    //
    //    } else if ("Dummy1".equalsIgnoreCase(user)) {
    //
    //    } else if ("Dummy1".equalsIgnoreCase(user)) {
    //
    //    } else if ("Dummy1".equalsIgnoreCase(user)) {
    //
    //    }
    return Arrays.asList("Learn Spring MVC", "Spring Boot KT", "Dance Now");
  }

}
