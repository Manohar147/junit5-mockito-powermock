package com.demo.junit5;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * TEST MUST ALWAYS BE PUBLIC
 */
class StringHelperTest {

  private StringHelper helper;

  @BeforeAll public static void beforeAll() {
    System.out.println(
        "---------------DO SOMETHING BEFORE RUNNING THE TEST---------------");
  }

  @BeforeEach public void afterEach() {
    helper = new StringHelper();
    System.out.println("DO SOMETHING BEFORE EACH TEST");
  }

  @Test public void testTruncateAInFirst2Positions_AinFirst2Positions() {
    assertEquals("CD", helper.truncateAInFirst2Positions("AACD"));
  }

  @Test public void testTruncateAInFirst2Positions_AinFirstPosition() {
    assertEquals("CD", helper.truncateAInFirst2Positions("ACD"));
  }

  @Test public void testAreFirstAndLastTwoCharactersTheSame() {
    assertTrue(helper.areFirstAndLastTwoCharactersTheSame("AB"));
  }

  @Test public void testArray() {
    int[] numbers = { 2, 1, 3, 4 };
    int[] expected = { 1, 2, 3, 4 };
    Arrays.sort(numbers);
    assertArrayEquals(expected, numbers);
  }

  @Test()
  public void testException() {
    assertThrows(NullPointerException.class, () -> {
      int[] numbers = null;
      Arrays.sort(numbers);
    });
  }

  @Test()
  public void testingPerformance() {
    assertTimeout(Duration.ofMillis(100), () -> {
      int[] array = {34, 5, 1, 36};
      for (int i = 0; i < 1000000; i++) {
        array[0] = i;
        Arrays.sort(array);
      }
    });
  }

  @Test()
  public void chainingAssertions() {
    List<String> list = Arrays.asList("alpha", "beta", "gamma");
    Assertions.assertAll("Some assertion message",
            () -> Assertions.assertEquals(list.get(0), "alpha"),
            () -> Assertions.assertEquals(list.get(1), "beta"),
            () -> Assertions.assertEquals(list.get(2), "gamma"));
  }

  @ParameterizedTest
  @CsvSource({"AACD,CD",
          "ACD,CD"})
  public void ParameterizedTestOne(
          String input, String expected) {
    assertEquals(expected, helper.truncateAInFirst2Positions(input));
  }

  @ParameterizedTest
  @ValueSource(strings = {"Hello",
          "World"})
  public void ParameterizedTestTwo(
          String message) {
    assertNotNull(message);
  }

  @AfterEach
  public void beforeEach() {
    helper = null;
    System.out.println("DO SOMETHING AFTER EACH TEST");
  }

  @AfterAll
  public static void afterAll() {

    System.out.println(
            "---------------DO SOMETHING AFTER RUNNING THE TEST---------------");

  }

}