package com.demo.hamcrestmatchers;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class HamcrestMatchersTest {

  @Test public void test() {
    List<Integer> scores = Arrays.asList(99, 100, 101, 105);

    assertThat(scores, hasSize(4)); //scores has 4 items
    assertThat(scores, hasItems(99, 100)); //scores has 99 and 100

    //every item greater than 90
    assertThat(scores, everyItem(greaterThan(90)));
    assertThat(scores, everyItem(lessThan(190)));

    //String
    assertThat("", is(emptyString()));
    assertThat("", is(emptyOrNullString()));
    //Arrays
    Integer[] marks = { 1, 2, 3 };
    assertThat(marks, arrayWithSize(3));
    assertThat(marks, arrayContaining(1, 2, 3));
    assertThat(marks, arrayContainingInAnyOrder(2, 1, 3));
  }
}