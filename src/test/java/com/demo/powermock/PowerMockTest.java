package com.demo.powermock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * MOCKING STATIC METHODS
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({UtilityClass.class,
        SystemUnderTest.class})
public class PowerMockTest {

    @Mock
    public Dependency dependencyMock;

    @InjectMocks
    public SystemUnderTest systemUnderTest;

    @Test
    public void powerMockito_MockingAStaticMethodCall() {
        //USE MOCKITO FOR THE SAME
        when(dependencyMock.retrieveAllStats()).thenReturn(Arrays.asList(1, 2, 3));

        PowerMockito.mockStatic(UtilityClass.class);

        when(UtilityClass.staticMethod(anyLong())).thenReturn(150);

        assertEquals(150, systemUnderTest.methodCallingAStaticMethod());

        //To verify a specific method call
        //First : Call PowerMockito.verifyStatic()
        //Second : Call the method to be verified
        PowerMockito.verifyStatic(UtilityClass.class);
        UtilityClass.staticMethod(6);

        // verify exact number of calls
        PowerMockito.verifyStatic(UtilityClass.class, Mockito.times(1));
        UtilityClass.staticMethod(6);
    }
    @Test
    public void testPrivateMethod() throws Exception {
        when(dependencyMock.retrieveAllStats()).thenReturn(Arrays.asList(1, 2, 3));

        long value = Whitebox
                .invokeMethod(systemUnderTest, "privateMethodUnderTest");
        assertEquals(6, value);
    }

    @Test
    public void powerMockito_MockingAConstructor() throws Exception {
        ArrayList mockList = mock(ArrayList.class);
        Mockito.when(mockList.size()).thenReturn(100);

        PowerMockito.whenNew(ArrayList.class).withAnyArguments()
                .thenReturn(mockList);

        int size = systemUnderTest.methodUsingAnArrayListConstructor();

        assertEquals(100, size);
    }

    @Test
    public void mockingStaticMethodWithMockito() {
        when(dependencyMock.retrieveAllStats()).thenReturn(Arrays.asList(1, 2, 3));
        try (MockedStatic<UtilityClass> utilityClassMockedStatic = mockStatic(UtilityClass.class)) {
            utilityClassMockedStatic.when(() -> UtilityClass.staticMethod(anyLong())).thenReturn(150);
            assertEquals(150, systemUnderTest.methodCallingAStaticMethod());
        }

    }
}
