package com.demo.mockito;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.*;

public class ListTest {

  @Test public void mockListSize() {
    List list = mock(List.class);
    when(list.size()).thenReturn(2);
    assertEquals(2, list.size());
  }

  @Test public void mockMultipleValues() {
    List list = mock(List.class);
    when(list.size()).thenReturn(2).thenReturn(3);
    assertEquals(2, list.size());
    assertEquals(3, list.size());
  }

  @Test public void mockListGet() {
    List list = mock(List.class);
    when(list.get(0)).thenReturn("ZERO")
            .thenReturn("ZERO_ONE"); //RETURN MULTIPLE VALUES
    assertEquals("ZERO", list.get(0));
    assertEquals("ZERO_ONE", list.get(0));
    assertEquals(null, list.get(1)); //RETURNS NULL FOR NON STUBED METHODS
  }

  @Test public void argumentMatcher() {
    List list = mock(List.class);
    when(list.get(anyInt())).thenReturn("ZERO");
    assertEquals("ZERO", list.get(0));
    assertEquals("ZERO", list.get(1)); //RETURNS NULL FOR NON STUBED METHODS
  }

  @Test public void mockThrowException() {
    List list = mock(List.class);
    when(list.get(anyInt()))
        .thenThrow(new RuntimeException("Something went wrong"));
    assertThrows(RuntimeException.class, () -> {
      list.get(0);
    });
  }

  @Test public void mixingUp() {
    List list = mock(List.class);
    //    when(list.subList(anyInt(),5)) //cant do this either be generic or be precise
    //        .thenThrow(new RuntimeException("Something went wrong"));
    when(list.subList(anyInt(),
                      anyInt())) //cant do this either be generic or be precise
        .thenThrow(new RuntimeException("Something went wrong"));
    assertThrows(RuntimeException.class, () -> {
      list.subList(0, 1);
    });
  }

  //BDD STYLE GIVEN WHEN THEN
  @Test void bddStyleForList() {

    //GIVEN
    List<String> list = mock(List.class);
    given(list.get(anyInt())).willReturn("ZERO");

    //WHEN
    String element = list.get(0);

    //THEN
    assertThat(element, is("ZERO"));
  }

}
