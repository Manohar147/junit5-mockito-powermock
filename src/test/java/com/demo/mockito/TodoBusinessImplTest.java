package com.demo.mockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.*;

/**
 * MOCKITO DOES NOW ALLOW STUBBING OF PRIVATE AND STATIC  METHODS & FINAL CLASSES AS MOKITO WANTS TO PROMOTE GOOD DESIGN
 * MOCKITO CAN MOCK BOTH CLASSES & INTERFACES
 * https://github.com/in28minutes/MockitoTutorialForBeginners
 * MockitoExtension makes sure mocks are created and injected into SUT(System Under Test).
 */

@ExtendWith(MockitoExtension.class) public class TodoBusinessImplTest {

  @Mock public TodoService todoServiceMock;

  @InjectMocks public TodoBusinessImpl todoBusiness; //SUT

  @Captor public ArgumentCaptor<String> stringArgumentCaptor;

  /**
   * Without annotations
   */
  @Test public void withoutAnnotations() {
    TodoService todoService = mock(
        TodoService.class); //CAN BE REPLACES WITH @Mock
    List<String> allTodos = Arrays
        .asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");
    when(todoService.retrieveTodos("Ranga")).thenReturn(allTodos);
    TodoBusinessImpl todoBusinessImpl = new TodoBusinessImpl(
        todoService); //CAN BE REPLACED WITH @InjectMocks
    List<String> todos = todoBusinessImpl.retrieveTodosRelatedToSpring("Ranga");
    assertEquals(2, todos.size());
  }

  /**
   * Without annotations
   */
  @Test public void mockInMethod(@Mock TodoService todoService) {
    List<String> allTodos = Arrays
        .asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");
    when(todoService.retrieveTodos("Ranga")).thenReturn(allTodos);
    TodoBusinessImpl todoBusinessImpl = new TodoBusinessImpl(
        todoService); //CAN BE REPLACED WITH @InjectMocks
    List<String> todos = todoBusinessImpl.retrieveTodosRelatedToSpring("Ranga");
    assertEquals(2, todos.size());
  }

  /**
   Here we are mocking the retrieveTodos method to return a certain result.
   */
  @Test public void basic() {
    when(todoServiceMock.retrieveTodos("Ranga")).thenReturn(
        Arrays.asList("Learn Spring MVC", "Learn Spring", "Learn to Dance"));
    List<String> todos = todoBusiness.retrieveTodosRelatedToSpring("Ranga");
    assertEquals(2, todos.size());
  }

  /**
   * BDD: Behaviour Driven Development
   * HERE we split the test case into GIVEN, WHEN & THEN scenarios
   * GIVEN IS THE SETUP FOR TEST
   * WHEN IS THE ACTUAL IMPLEMENTATION OR METHOD CALL
   * THEN IS WHEN WE ASSERT
   */
  @Test public void behaviourDrivenDevelopmentBDD() {
    //GIVEN - SETUP
    List<String> allTodos = Arrays
        .asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");
    given(todoServiceMock.retrieveTodos("Ranga"))
        .willReturn(allTodos); //use given for given part
    //WHEN ACTUAL METHOD CALL
    List<String> todos = todoBusiness.retrieveTodosRelatedToSpring("Ranga");
    //THEN ASSERT
    assertThat(todos.size(), is(2));
  }

  /**
   * Here we can verify mocked methods and check if the method was actually called ect..
   */
  @Test public void verifyMockedMethods() {
    List<String> allTodos = Arrays
        .asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");
    given(todoServiceMock.retrieveTodos("Ranga"))
        .willReturn(allTodos); //use given for given part
    List<String> todos = todoBusiness.retrieveTodosRelatedToSpring("Ranga");

    verify(todoServiceMock).retrieveTodos(
        "Ranga"); //VERIFY method retrieveTodos is called with parameter Ranga
    //    verify(todoService).retrieveTodos("BABU"); //WILL FAIL
    verify(todoServiceMock, never()).retrieveTodos("BABU");
    //VERIFY method retrieveTodos is never called with parameter BABU
    verify(todoServiceMock, times(1)).retrieveTodos(
        "Ranga"); //VERIFY method retrieveTodos is called with parameter Ranga is called once

    verify(todoServiceMock, atLeast(1)).retrieveTodos(
        "Ranga"); //VERIFY method retrieveTodos is called with parameter Ranga is called atleast once

    verify(todoServiceMock, atLeastOnce()).retrieveTodos(
        "Ranga"); //VERIFY method retrieveTodos is called with parameter Ranga is called atleast once
  }

  /**
   * BDD Style of verifying the test
   */
  @Test public void verifyMockedMethodsBDD() {
    //GIVEN - SETUP
    List<String> allTodos = Arrays
        .asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");
    given(todoServiceMock.retrieveTodos("Ranga"))
        .willReturn(allTodos); //use given for given part
    //WHEN ACTUAL METHOD CALL
    List<String> todos = todoBusiness.retrieveTodosRelatedToSpring("Ranga");
    //THEN ASSERT
    assertThat(todos.size(), is(2));

    then(todoServiceMock).should().retrieveTodos("Ranga");

    then(todoServiceMock).should(never()).retrieveTodos("BABU");

    then(todoServiceMock).should(times(1)).retrieveTodos("Ranga");

    then(todoServiceMock).should(atLeast(1)).retrieveTodos("Ranga");
    then(todoServiceMock).should(atLeastOnce()).retrieveTodos("Ranga");

  }

  /**
   * Argument Capture is used to check what argument is passed to a mocked method.
   * In this example TodoBusinessImpl makes use of TodoService mock and TodoBusinessImpl.retrieveTodosRelatedToSpring() method calls
   * the TodoService.retrieveTodos method. we would like to know what was the argument passed to this TodoService.retrieveTodos method.
   * We can use argument capture to do the same
   */
  @Test public void argumentCapture() {
    //DECLARE ARGUMENT CAPTURE

    //GIVEN - SETUP
    List<String> allTodos = Arrays
        .asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");
    given(todoServiceMock.retrieveTodos("Ranga"))
        .willReturn(allTodos); //use given for given part
    //WHEN ACTUAL METHOD CALL
    List<String> todos = todoBusiness.retrieveTodosRelatedToSpring("Ranga");
    //THEN
    then(todoServiceMock).should()
        .retrieveTodos(stringArgumentCaptor.capture());

    assertThat(stringArgumentCaptor.getValue(), is("Ranga"));

  }

  /**
   * Multiple Argument Captures Here we call the method 2 times.
   */
  @Test public void argumentCaptureMultiple() {
    //DECLARE ARGUMENT CAPTURE
    ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor
        .forClass(String.class);

    //GIVEN - SETUP
    List<String> allTodos = Arrays
        .asList("Learn Spring MVC", "Learn Spring", "Learn to Dance");
    given(todoServiceMock.retrieveTodos("Ranga"))
        .willReturn(allTodos); //use given for given part
    given(todoServiceMock.retrieveTodos("Babu")).willReturn(allTodos);
    //WHEN ACTUAL METHOD CALL
    List<String> todos = todoBusiness.retrieveTodosRelatedToSpring("Ranga");
    List<String> todos1 = todoBusiness.retrieveTodosRelatedToSpring("Babu");
    //THEN
    then(todoServiceMock).should(times(2))
        .retrieveTodos(stringArgumentCaptor.capture());

    assertThat(stringArgumentCaptor.getAllValues().size(), is(2));

  }

  /**
   NAMING TEST METHOD <method_name>_<scenario>_<result>
   ALWAYS TRY TO DIVIDE THE TEST CASE TO GIVEN WHEN THEN STATEMENTS
   */
  @Test public void retrieveTodosRelatedToSpring_getALlTodosWithSpring_shouldGet2Results() {
    //GIVEN
    given(todoServiceMock.retrieveTodos("Jhon")).willReturn(
        Arrays.asList("Learn Spring MVC", "Learn Spring", "Learn to Dance"));
    //WHEN
    List<String> todos = todoBusiness.retrieveTodosRelatedToSpring("Jhon");
    //THEN
    assertThat(todos.size(), is(2));
    verify(todoServiceMock).retrieveTodos("Jhon"); //NORMAL STYLE
    then(todoServiceMock).should().retrieveTodos("Jhon"); //BDD STYLE
    verify(todoServiceMock, atLeastOnce()).retrieveTodos("Jhon"); //NORMAL STYLE
    then(todoServiceMock).should(atLeastOnce()).retrieveTodos("Jhon");
  }

}