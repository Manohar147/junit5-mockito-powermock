package com.demo.spy;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

public class SpyTest {
  @Test public void test() {
    List l = spy(ArrayList.class);
    assertEquals(0, l.size());
    l.add("Dummy");
    l.add("Mummy");
    assertEquals(2, l.size());
    l.remove("Dummy");
    assertEquals(1, l.size());
    when(l.size()).thenReturn(5);
    assertEquals(5, l.size());

  }

  @Test public void test1() {
    List<String> l = spy(ArrayList.class);
    l.add("Dummy");
    l.add("Mummy");
    then(l).should(never()).remove(anyString());
    verify(l, never()).remove(anyString());
    then(l).should(never()).clear();
    verify(l, never()).clear();
  }
}