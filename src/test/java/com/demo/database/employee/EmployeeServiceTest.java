package com.demo.database.employee;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("EmployeeServiceTest: Test All employee related methods")
class EmployeeServiceTest {

    private static List<Employee> employees;

    @Mock
    public EmployeeDAO employeeDAO;

    @InjectMocks
    public EmployeeService employeeService;

    @BeforeAll
    public static void beforeAll() throws IOException {
        employees = new ObjectMapper().readValue(new File("src/test/resources/employees.json"),
                new TypeReference<List<Employee>>() {
                });
    }

    @AfterAll
    public static void afterAll() {
        employees = null;
    }

    @BeforeEach
    public void beforeEach() throws IOException {
    }

    @Test
    @DisplayName("Get Employees whose age is greater than 50")
    void getEmployeesWithAgeGreaterThan_getEmployeesWhoseAgeIsGreaterThan50_shouldGetOneEmployee() {
        given(employeeDAO.getEmployees()).willReturn(employees);
        List<Employee> filteredEmployees = employeeService.getEmployeesWithAgeGreaterThan(50);
        assertThat(filteredEmployees, hasSize(1));
        then(employeeDAO).should(times(1)).getEmployees();
    }

    @Test
    @DisplayName("Get Employees whose age is less than 50")
    void getEmployeesWithAgeLessThan_getEmployeesWhoseAgeIsLessThan50_shouldGetTwoEmployees() {
        given(employeeDAO.getEmployees()).willReturn(employees);
        List<Employee> filteredEmployees = employeeService.getEmployeesWithAgeLessThan(50);
        assertThat(filteredEmployees, hasSize(2));
        then(employeeDAO).should(times(1)).getEmployees();
    }

    @Test
    @DisplayName("Get Employee whose age is equal to 55")
    void getEmployeesWithAgeEqualTo_getEmployeeWhoseAgeIsEqualTo55_shouldGetAnEmployee() {
        given(employeeDAO.getEmployees()).willReturn(employees);
        Employee employee = employeeService.getEmployeesWithAgeEqualTo(55);
        assertThat(employee, is(notNullValue()));
        then(employeeDAO).should(times(1)).getEmployees();
    }

    @ParameterizedTest
    @ValueSource(ints = {29, 39, 55})
    @DisplayName("Get Employee whose age is equal to {29, 39, 55}")
    void getEmployeesWithAgeEqualTo_getEmployeeWhoseAgeIsEqualToValue_shouldGetAnEmployee(int age) {
        given(employeeDAO.getEmployees()).willReturn(employees);
        Employee employee = employeeService.getEmployeesWithAgeEqualTo(age);
        assertThat(employee, is(notNullValue()));
        then(employeeDAO).should(times(1)).getEmployees();
    }

    @Test
    @DisplayName("Get Employee whose id is equal to 55215")
    @Disabled
    void getEmployeesWithAgeEqualTo_getEmployeeWhoseIdEqualTo55215_shouldGetAnEmployee() {
        given(employeeDAO.getEmployees()).willReturn(employees);
        Employee employee = employeeService.getEmployeesWithAgeEqualTo(55);
        assertThat(employee, is(notNullValue()));
        then(employeeDAO).should(times(1)).getEmployees();
    }

    @AfterEach
    public void afterEach() throws IOException {
    }
}