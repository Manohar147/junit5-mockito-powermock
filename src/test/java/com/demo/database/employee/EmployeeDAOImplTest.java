package com.demo.database.employee;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.when;

@ExtendWith(MockitoExtension.class)
class EmployeeDAOImplTest {

    @Mock
    public DataSource dataSource;

    @Mock
    public Connection connection;

    @Mock
    public ResultSet resultSet;

    @Mock
    public PreparedStatement statement;


    @InjectMocks
    public EmployeeDAOImpl employeeDAO;

    @Test
    void getEmployees() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(statement.getResultSet()).thenReturn(resultSet);
        when(connection.prepareStatement("SELECT * from employees")).thenReturn(statement);
        given(dataSource.getConnection()).willReturn(connection);
        List<Employee> employees = employeeDAO.getEmployees();
        assertThat(employees, hasSize(2));
    }
}