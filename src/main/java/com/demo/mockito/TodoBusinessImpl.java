package com.demo.mockito;

import java.util.ArrayList;
import java.util.List;

/**
 * SUT - SYSTEM UNDER TEST
 */
public class TodoBusinessImpl {
  private TodoService todoService;

  public TodoBusinessImpl(TodoService todoService) {
    this.todoService = todoService;
  }

  public List<String> retrieveTodosRelatedToSpring(String user) {
    List<String> filteredTodos = new ArrayList();
    List<String> allTodos = todoService.retrieveTodos(user);
    for (String todo : allTodos) {
      if (todo.contains("Spring")) {
        filteredTodos.add(todo);
      }
    }
    return filteredTodos;
  }
}
