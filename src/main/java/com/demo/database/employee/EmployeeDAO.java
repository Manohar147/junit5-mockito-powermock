package com.demo.database.employee;

import java.util.List;

public interface EmployeeDAO {
    List<Employee> getEmployees();
}
