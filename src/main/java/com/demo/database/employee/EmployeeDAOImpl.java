package com.demo.database.employee;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAOImpl implements EmployeeDAO {
    //@Autowired
    private final DataSource dataSource;

    public EmployeeDAOImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Employee> getEmployees() {
        List<Employee> employees = null;
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from employees");
            ResultSet resultSet = preparedStatement.getResultSet();
            employees = new ArrayList();
            while (resultSet.next()) {
                Employee employee = new Employee();
                employee.setName(resultSet.getString(1));
                employee.setAge(resultSet.getInt(2));
                employee.setRole(resultSet.getString(3));
                employees.add(employee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employees;
    }
}
