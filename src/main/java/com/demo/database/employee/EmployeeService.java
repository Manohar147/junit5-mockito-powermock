package com.demo.database.employee;

import java.util.List;
import java.util.stream.Collectors;

public class EmployeeService {
    //@Autowired
    private EmployeeDAO employeeService;

    public EmployeeService() {
    }

    public List<Employee> getEmployeesWithAgeGreaterThan(int age) {
        return employeeService.getEmployees().stream()
                .filter(employee -> employee.getAge() > age)
                .collect(Collectors.toList());
    }

    public List<Employee> getEmployeesWithAgeLessThan(int age) {
        return employeeService.getEmployees().stream()
                .filter(employee -> employee.getAge() < age)
                .collect(Collectors.toList());
    }

    public Employee getEmployeesWithAgeEqualTo(int age) {
        return employeeService.getEmployees().stream()
                .filter(employee -> employee.getAge() == age).findFirst().orElse(null);
    }
}
